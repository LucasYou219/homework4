/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regiovincomapeditor.gui;

import javafx.scene.layout.Pane;
import regiovincomapeditor.RegioVincoMapEditor;
import saf.components.AppWorkspaceComponent;
import saf.ui.AppGUI;

/**
 *
 * @author li zhao
 */
public class Workspace extends AppWorkspaceComponent 
{
    RegioVincoMapEditor app;

    public Workspace(RegioVincoMapEditor initApp) 
    {
        app = initApp;
        workspace = new Pane();
    }
    
    
    @Override
    public void reloadWorkspace() {
        
    }
    
    
    @Override
    public void initStyle() {
        
    }
    
}
